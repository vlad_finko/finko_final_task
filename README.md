==================== Requirements ====================

1.
Create certificate for your domain inside ACM and paste certificate arn into ALB resource

2.
Create IAMRole MyIAMRoleForSSM with following policy:
- arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM
- arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds

3.
Add AWS Key Pairs "mykey" and save private key.

==================== Terraform ====================

1.
Go to Application/terraform/ and run:
 - terraform init
 - terraform plan
 - terraform apply

2.
From terraform output copy RDS endpoint to wp-config.php
define( 'DB_HOST', 'terraform-20211125181958274400000001.ciglbkvsg5td.eu-central-1.rds.amazonaws.com' );

3.
Push changes to GitLab


==================== Jenkins ====================


1.
Open Jenkins in browser on 8080 port on ec2 public ip.

2.
Open Jenkins terminal in AWS Session Manager and run:
cat /var/lib/jenkins/secrets/initialAdminPassword

3.
Install suggested plugins

4.
Create First Admin User
admin / password!

5.
Install plugin:
- SSH agent

6.
Add Credentials (private key from "mykey") for SSH access from Jenkins instance to application instance.
- Use ec2-user inside username

7.
Copy credentials id to Jenkinsfile inside Deployment step.
example id: 8dd8daf2-1001-4c71-9e01-4035c3cb42ee

8.
Add new Job type Pipeline:
- Type: "Pipeline script from SCM"
- Git: https://gitlab.com/vlad_finko/finko_final_task
- Use main branch instead of master

9.
Update Jenkinsfile:
- Put private ip into value APPLICATION_IP

10.
Run pipeline

==================== Wordpress pre configuration ====================

1.
In browser open public ip of ec2 instance. Yo will be redirected to  http://18.192.20.29/wp-admin/install.php

2.
Write init information for install wordpress and load database with init wordpress data.

3.
Wait 3-5 minutes and check healthcheks inside target groups. You will found healthy instance.

4.
Check https://skyrockets.uk



==================== CI/CD Check ====================

1.
Update title inside Application/wordpress/index.php

<html>
  <head>
    <meta charset="utf-8"/>
    <title>Version 2</title>
  </head>
</html>

2.
Add index.php to git and push to GitLab

3.
Run new build in Jenkins

4.
Check https://skyrockets.uk

