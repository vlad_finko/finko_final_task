resource "aws_route53_record" "alias_route53_record" {
  zone_id = var.aws_route53_zone
  name    = var.dns_name
  type    = "A"

  alias {
    name                   = aws_lb.alb.dns_name
    zone_id                = aws_lb.alb.zone_id
    evaluate_target_health = true
  }
}