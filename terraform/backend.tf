terraform {
  backend "s3" {
    bucket = "devopsacademy-terraform-state-new"
    key    = "terraform.state"
    region = "eu-central-1"
  }
}
