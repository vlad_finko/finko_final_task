resource "aws_iam_role" "IAMRoleForSSM" {
  name = "IAMRoleForSSM"
  assume_role_policy  = file("role.json")
  tags = {
    tag-key = "tag-value"
  }
 }

 resource "aws_iam_role_policy" "policy_one" {
   name = "IAMRoleForSSM"
   role =  aws_iam_role.IAMRoleForSSM.id
   policy =  file("iam-policy.json")
 }

  resource "aws_iam_role_policy" "policy_two" {
    name = "IAMRoleForECR"
    role =  aws_iam_role.IAMRoleForSSM.id
    policy =  file("iam-policy-ecr.json")
  }

  resource "aws_iam_instance_profile" "test_profile" {
  name = "IAMRoleForSSM"
  role = aws_iam_role.IAMRoleForSSM.name
  }
