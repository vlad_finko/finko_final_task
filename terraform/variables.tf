variable "ami_id" {
  default = "ami-0bd99ef9eccfee250"
}

variable "application_instance_type" {
  default = "t2.micro"
}

variable "jenkins_instance_type" {
  default = "t2.medium"
}

variable "aws_route53_zone" {
  default = "Z02820282PEMLUSGA7BRI"
}

variable "dns_name" {
  default = "skyrockets.uk"
}

variable "cidr_block" {
  default = "10.0.0.0/16"
}

variable "cidr_block_subnet1" {
  default = "10.0.1.0/24"
}

variable "cidr_block_subnet2" {
  default = "10.0.2.0/24"
}

variable "db_username" {
  default = "user"
}

variable "db_password" {
  default = "change_me"
}

variable "db_name" {
  default = "wordpress"
}

variable "ports_app" {
  type    = list(string)
default = ["80", "22"]
}

variable "ports_jenkins" {
type    = list(string)
default = ["8080", "22"]
}



