resource "aws_vpc" "vpc" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"

  tags = {
    Creator = "Finko"
    Name    = "VPC"
  }
}

resource "aws_internet_gateway" "internet-gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Creator = "Finko"
    Name    = "Internet-gateway"
  }
}

resource "aws_subnet" "public-subnet1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidr_block_subnet1
  availability_zone_id    = "euc1-az1"

  map_public_ip_on_launch = true

  tags = {
    Creator = "Finko"
    Name    = "Public-subnet1"
  }
}

resource "aws_subnet" "public-subnet2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidr_block_subnet2
  availability_zone_id    = "euc1-az2"

  map_public_ip_on_launch = true

  tags = {
    Creator = "Finko"
    Name    = "Public-subnet2"
  }
}


resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gateway.id
  }


  tags = {
    Creator = "Finko"
    Name    = "Public-route-table"
  }
}

resource "aws_route_table_association" "public-subnet-route-table-association1" {
  subnet_id      = aws_subnet.public-subnet1.id
  route_table_id = aws_route_table.public-route-table.id
}

resource "aws_route_table_association" "public-subnet-route-table-association2" {
  subnet_id      = aws_subnet.public-subnet2.id
  route_table_id = aws_route_table.public-route-table.id
}