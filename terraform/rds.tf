resource "aws_db_subnet_group" "database-subnet-group" {
  name       = "database_subnet"
  subnet_ids = [aws_subnet.public-subnet1.id, aws_subnet.public-subnet2.id]
  tags = {
    Creator = "Finko"
    Name    = "Database-subnet-group"
  }
}

resource "aws_db_instance" "db_instance" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = var.db_name
  username               = var.db_username
  password               = var.db_password
  db_subnet_group_name   = aws_db_subnet_group.database-subnet-group.name
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.db-security-group.id]

  tags = {
    Creator = "Finko"
    Name    = "RDSInstance"
  }
}


resource "aws_security_group" "db-security-group" {
  name        = "db-security_group"
  description = "security-group-for-rds"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "MySQL port for connect from EC2 instance"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    security_groups  = [aws_security_group.application.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Finko"
    Name    = "Security-group-for-rds"
  }
}
