resource "aws_instance" "application-instance" {
  ami                    = var.ami_id
  instance_type          = var.application_instance_type
  vpc_security_group_ids = [aws_security_group.application.id]
  subnet_id              = aws_subnet.public-subnet1.id
  key_name               = "newkey"
  iam_instance_profile   = aws_iam_instance_profile.test_profile.name

  tags = {
    Creator = "Finko"
    Name    = "application-Finko"
  }
  user_data = file("user_data_app.sh")
}


resource "aws_key_pair" "key-pair" {
  key_name   = "newkey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1nB7j823hAsVTSWRhJQ8LeO+2c/+KLaD6k4e33nayL6BggSx9KX1dtUJIv7HIeg4vtJno149qaqw36uKXkhF/TSzVvMRmZ99XLbbBrdafnn1fZ0rJ+vjLtDi1PpcaTwRQkd0hhVovilSvuZ57fVYMd2hhCn2RoH48ZRIwI9aAj6clJdu1nh1lVHAePWiVhslNTc2/ak8cU/G1BwhOre0Sl1y5uiiUgPF7ZgaCwsLNrOSTydJjkICqEVvvS6J0apwB1by+MvV/cXNPAb/9Ha0m3tN2U1WYTeUdzKuAFNpCFIe1X6DRNySlR6z+ONtTEWb9/pZR8L1MqNn6JR6C7ueb"
}


resource "aws_security_group" "application" {
  name        = "application-security_group"
  description = "application-security_group"
  vpc_id      = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.ports_app
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Finko"
    Name    = "Application-security_group"
  }
}

resource "aws_instance" "jenkins-instance" {
  ami                    = var.ami_id
  instance_type          = var.jenkins_instance_type
  subnet_id              = aws_subnet.public-subnet1.id
  vpc_security_group_ids = [aws_security_group.jenkins-security-group.id]
  key_name               = "newkey"
  iam_instance_profile   = aws_iam_instance_profile.test_profile.name

  tags = {
    Creator = "Finko"
    Name    = "jenkins-Finko"
  }
  user_data = file("user_data_jenkins.sh")
}

resource "aws_security_group" "jenkins-security-group" {
  name = "jenkins-security-group"
  description = "jenkins-security-group"
  vpc_id = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.ports_jenkins
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Creator = "Finko"
    Name    = "Jenkins-security_group"
  }
}

resource "aws_ecr_repository" "ecr" {
  name                 = "wordpress"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}





