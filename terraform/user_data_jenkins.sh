#!/bin/bash 
sudo su
amazon-linux-extras install epel -y
yum update -y
yum -y  install java-1.8.0 git
wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
yum -y install jenkins
systemctl start jenkins
amazon-linux-extras install docker
usermod -a -G docker ec2-user
usermod -a -G docker jenkins
chkconfig docker on
service docker start
systemctl restart jenkins